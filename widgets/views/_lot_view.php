<?php

/** @var $dataProvider \yii\data\ActiveDataProvider */

use yii\helpers\Html;
use yii\helpers\Url;

$this->registerCssFile('/css/lot.css', ['rel' => 'stylesheet'], 'lotStyle');
$counter = 1;
?>
<?php /** @var \app\models\Lot $model */
foreach ($dataProvider->getModels() as $model): ?>
    <?php if (($counter % 2) != 0): ?>
        <div class="row">
    <?php endif; ?>
    <div class="col-md-6">
        <div class="lot-card">
            <div class="row">
                <!--    Основная часть-->
                <div class="col-sm-10">
                    <div class="row header-lot">
                        <div class="col-md-8 header-lot-info">
                            <div class="lot-info-name">
                                <div class="btn btn-xs btn-icon btn-circle btn-success"></div>
                                <p><?= Html::a(mb_substr($model->name, 0, 20), ['/lot/view', 'id' => $model->id]) ?></p>
                                <small>(<?= $model->lotStatus ? str_replace(' ', '&nbsp;',
                                        $model->lotStatus->name) : 'не указан'; ?>)
                                </small>
                            </div>
                            <div class="lot-info-other">
                                <?= Html::a('', Url::to($model->tbankrot_link, true), [
                                    'class' => 'link-tbankrot fa fa-external-link',
                                    'title' => 'tbankrot.ru',
                                    'target' => '_blank'
                                ]) ?>

                                <p><?= Html::a('ЭТП', Url::to($model->auction_link, true), [
                                        'target' => '_blank'
                                    ]) ?></p>

                            </div>
                        </div>
                        <div class="col-md-4 total-cost text-right">
                            <p><?= Yii::$app->formatter->asCurrency($model->auction_price, 'rub'); ?></p>
                        </div>
                    </div>
                    <div class="row body-lot">
                        <div class="body-lot-description">
                            <div><?= mb_substr($model->description, 0, 200); ?>...</div>
                        </div>
                    </div>
                    <div class="row footer-lot">
                        <div class="col-md-12 footer-lot-publish-date text-right">
                            <span>Дата окончания:&nbsp;</span>
                            <p><?= Yii::$app->formatter->asDatetime($model->end_date); ?></p>
                        </div>
                    </div>
                </div>
                <!--    Управляющие кнопки-->
                <div class="col-sm-2 body-lot-buttons">
                    <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'role' => 'modal-remote',
                        'title' => 'Удалить',
                        'data-toggle' => 'tooltip',
                        'data-confirm' => false,
                        'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                        'data-confirm-title' => 'Подтверждаете удаление?',
                        'data-confirm-message' => 'Подтвердите выполенение операции удаления',
                        'data-confirm-ok' => 'Удалить',
                        'data-confirm-cancel' => 'Отмена',

                    ]) ?>
                    <?= Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id], [
                        'class' => 'btn btn-info',
                        'role' => 'modal-remote',
                        'title' => 'Редактировать лот',
                        'data-toggle' => 'tooltip',
                    ]) ?>
                    <?= Html::a('<i class="fa fa-eye"></i>', ['view', 'id' => $model->id], [
                        'class' => 'btn btn-success',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <?php if (($counter % 2) == 0): ?>
        </div>
    <?php endif; ?>
<?php
$counter++;
endforeach; ?>

