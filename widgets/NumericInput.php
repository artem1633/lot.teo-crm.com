<?php

namespace app\widgets;

use app\assets\plugins\AutoNumericAsset;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\InputWidget;
use yii\helpers\ArrayHelper;

/**
 * Class NumericInput
 * @package app\widgets
 */
class NumericInput extends InputWidget
{
    public $pluginOptions;

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        AutoNumericAsset::register($this->view);

        $script = "
            $('#{$this->id}').autoNumeric('init');
        ";

        $this->view->registerJs($script, View::POS_READY);

        $this->options = ArrayHelper::merge(['id' => $this->id, 'class' => 'form-control'], $this->pluginOptions);

        echo Html::activeInput('text', $this->model, $this->attribute, $this->options);
//        return $this->field->textInput(ArrayHelper::merge(['id' => $this->id], $this->pluginOptions))->label(false);
    }
}