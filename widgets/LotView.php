<?php

namespace app\widgets;

use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

/**
 * Class LotsView
 * @package app\widgets
 */
class LotView extends Widget
{
    /**
     * @var ActiveDataProvider
     */
    public $dataProvider;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if($this->dataProvider == null){
            throw new InvalidConfigException('Data provider must be set');
        }
        parent::init();
    }


    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        return $this->render('_lot_view', ['dataProvider' => $this->dataProvider]);
    }

}