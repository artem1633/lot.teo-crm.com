<?php

/* @var $this yii\web\View */
/* @var $model app\models\LotTask */
?>
<div class="lot-task-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
