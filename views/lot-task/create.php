<?php

/* @var $this yii\web\View */
/* @var $model app\models\LotTask */

?>
<div class="lot-task-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
