<?php

use app\models\User;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

/** @var User $identity */
$identity = Yii::$app->user->identity;
?>

<div class="lot-task-list">
    <?php /** @var \app\models\LotTask $model */
    foreach ($dataProvider->getModels() as $model): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="item-task">
                    <div class="item-task-text">
                        <p> <?php if ($model->done): ?>
                              <p> <i class="fa fa-check" style="color: green;"></i>&nbsp;Выполнено</p>
                        <?php endif; ?>
                        <p><?= $model->text ?></p>
                    </div>
                    <!--Кнопки-->
                    <div class="item-task-btn">
                        <?= Html::a('<i class="fa fa-check"></i>',
                            ['/lot-task/done', 'id' => $model->id], [
                                'role' => 'modal-remote',
                                'title' => 'Завершить',
                                'data-toggle' => 'tooltip',
                                'style' => 'color: green;',
                                'hidden' => (bool)$model->done,
                            ]) ?>
                        &nbsp;
                        <?= Html::a('<i class="fa fa-pencil"></i>',
                            ['/lot-task/update', 'id' => $model->id], [
                                'role' => 'modal-remote',
                                'title' => 'Редактирование',
                                'data-toggle' => 'tooltip',
                                'hidden' => (bool)$model->done,
                            ]) ?>
                        &nbsp;
                        <?= Html::a('<i class="fa fa-trash"></i>',
                            ['/lot-task/delete', 'id' => $model->id], [
                                'style' => 'color: red;',
                                'role' => 'modal-remote',
                                'title' => 'Удаление',
                                'data-toggle' => 'tooltip',
                                'data-confirm' => false,
                                'data-method' => false,// for overide yii data api
                                'data-request-method' => 'post',
                                'data-confirm-title' => 'Удаление задачи',
                                'data-confirm-message' => 'Подвердите удаление задачи',
                                'data-confirm-ok' => 'Удалить',
                                'data-confirm-cancel' => 'Отмена',
                            ]) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
