<?php

use yii\helpers\Url;

/** @var \app\models\User $identity */
$identity = Yii::$app->user->identity;
return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
        'visible' => $full,

    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'company',
        'content' => function ($model) {
            return $model->lot->company ? $model->lot->company->name : null;
        },
        'visible' => $identity->isSuperAdmin(),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'text',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'filter' => [0 => 'Нет', 1 => 'Да'],
        'attribute' => 'done',
        'content' => function (\app\models\LotTask $model) {
            if ($model->done) {
                return 'Да';
            }
            return 'Нет';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'lot_id',
        'content' => function (\app\models\LotTask $model) {
            return $model->lot ? $model->lot->name : '';
        },
        'visible' => $full,
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => '{update} {delete}',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'
        ],
    ],

];   