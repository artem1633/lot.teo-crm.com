<?php

use app\models\Lot;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\models\LotTask */
/* @var $searchModel app\models\search\LotTaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var bool $full полное таблицы отображение или нет */
/* @var $form yii\widgets\ActiveForm */


//$this->title = 'Lot Tasks';
//$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="lot-task-index">
    <?php $form = ActiveForm::begin([
        'action' => '/lot-task/create'
    ]) ?>
    <div class="row">
        <div class="col-md-12">
            <?php if (Yii::$app->user->identity->isSuperAdmin()): ?>
                <?= $form->field($model, 'lot_id')->dropDownList((new Lot())->getList())->label(false); ?>
            <?php else: ?>
                <?= $form->field($model, 'lot_id')->hiddenInput()->label(false); ?>
            <?php endif; ?>
        </div>
        <div class="col-md-10">
            <?= $form->field($model, 'text')->textInput([
                'placeholder' => 'Введите текст задачи и нажмите ОК'
            ])->label(false) ?>
        </div>
        <div class="col-md-2">
            <?= Html::submitButton('Ок', [
                'class' => 'btn btn-info btn-block',
//                'role' => 'modal-remote'
            ]) ?>
        </div>
        <?= $form->field($model, 'done')->hiddenInput()->label(false); ?>
    </div>
    <?php ActiveForm::end() ?>
    <div class="row">
        <div class="col-md-12">
<!--            --><?php //Pjax::begin([
//                'id' => 'task-list-pjax',
//                'enablePushState' => false
//            ]) ?>
            <?= $this->render('_task_list', [
                'dataProvider' => $dataProvider
            ]) ?>
<!--            --><?php //Pjax::end() ?>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
