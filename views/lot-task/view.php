<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LotTask */
?>
<div class="lot-task-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'text',
            'done',
            'lot_id',
        ],
    ]) ?>

</div>
