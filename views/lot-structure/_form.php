<?php

use unclead\multipleinput\MultipleInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LotStructure */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lot-structure-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'structure')->widget(MultipleInput::className(), [
            'min' => 1,
            'columns' => [
                [
                    'name'  => 'item_name',
                ],
                [
                    'name'  => 'count',
                ],
                [
                    'name'  => 'min_price',
                ],
                [
                    'name'  => 'max_price',
                ],
                [
                    'name'  => 'comment',
                ],
                [
                    'name'  => 'comment',
                    'type'  => 'dropDownList',
                    'items' => $model->getStructureGroupList(),
                ],
            ]
        ]);
        ?>

    <?= $form->field($model, 'count')->textInput() ?>

    <?= $form->field($model, 'min_price')->textInput() ?>

    <?= $form->field($model, 'max_price')->textInput() ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'group_id')->textInput() ?>

    <?= $form->field($model, 'source_link')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'lot_id')->textInput() ?>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>

</div>
