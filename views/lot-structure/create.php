<?php

/* @var $this yii\web\View */
/* @var $model app\models\LotStructure */

?>
<div class="lot-structure-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
