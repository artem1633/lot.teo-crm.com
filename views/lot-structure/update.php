<?php

/* @var $this yii\web\View */
/* @var $model app\models\LotStructure */
?>
<div class="lot-structure-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
