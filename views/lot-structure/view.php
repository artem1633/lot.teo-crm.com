<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LotStructure */
?>
<div class="lot-structure-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'item_name',
            'count',
            'min_price',
            'max_price',
            'comment:ntext',
            'group_id',
            'source_link:ntext',
            'lot_id',
        ],
    ]) ?>

</div>
