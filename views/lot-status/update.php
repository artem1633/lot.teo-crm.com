<?php

/* @var $this yii\web\View */
/* @var $model app\models\LotStatus */
?>
<div class="lot-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
