<?php

/* @var $this yii\web\View */
/* @var $model app\models\LotStatus */

?>
<div class="lot-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
