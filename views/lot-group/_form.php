<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LotGroup */
/* @var $form yii\widgets\ActiveForm */

/** @var \app\models\User $identity */
$identity = Yii::$app->user->identity;
?>

<div class="lot-group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if ($identity->isSuperAdmin()): ?>
        <?= $form->field($model, 'company_id')->widget(\kartik\select2\Select2::class, [
            'data' => (new \app\models\Company())->getList(),
            'options' => [
                'prompt' => 'Выберите компанию',
            ]
        ]) ?>
    <?php else: ?>
        <?= $form->field($model, 'company_id')->hiddenInput(['value' => $identity->company_id])->label(false) ?>
    <?php endif; ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color')->input('color') ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
