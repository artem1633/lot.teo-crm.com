<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LotGroup */
?>
<div class="lot-group-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'color',
        ],
    ]) ?>

</div>
