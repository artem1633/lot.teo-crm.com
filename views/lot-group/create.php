<?php

/* @var $this yii\web\View */
/* @var $model app\models\LotGroup */

?>
<div class="lot-group-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
