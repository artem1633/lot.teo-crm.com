<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LotStructureGroup */
?>
<div class="lot-structure-group-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
