<?php

/* @var $this yii\web\View */
/* @var $model app\models\LotStructureGroup */

?>
<div class="lot-structure-group-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
