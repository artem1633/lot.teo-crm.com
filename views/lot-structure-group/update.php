<?php

/* @var $this yii\web\View */
/* @var $model app\models\LotStructureGroup */
?>
<div class="lot-structure-group-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
