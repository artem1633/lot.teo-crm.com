<?php

use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Lot */
/* @var $lot_task_model app\models\LotTask */
/* @var $structure_model app\models\LotStructure */
/* @var $lotTaskSearchModel app\models\search\LotTaskSearch */
/* @var $lotTaskDataProvider \yii\data\ActiveDataProvider */
$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;

$onchange = <<<JS
    var elem = $(this);
    var row = elem.parent('div').parent('td').parent('tr');
    var count = row.find('.count-item').val();
    var min_price = row.find('.min-price').val();
    var max_price = row.find('.max-price').val();
    var calculated_filed = row.find('.calculated-field');
    var min_sum = count * min_price;
    var max_sum = count * max_price;
    var mid_sum = (min_sum + max_sum)/2;
    
    calculated_filed.html(min_sum + '/' + max_sum + '/' + mid_sum);
    
    var name = elem.attr('data-name');
    var value = elem.val();
    var id = row.find('.ls-id').val();
     $.post(
        '/lot-structure/save-parameter',
        {id:id, name:name, value:value},
        function(res){
            console.log(res);
        }
    );
    return true;
    
JS;

?>
<div class="lot-view">
    <div class="row">
        <!--Левый блок-->
        <!--Инфо о лоте-->
        <div class="col-md-5">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Информация
                    </h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                        <?= Html::a('Составить список лота', ['structure', 'id' => $model->id], [
                            'class' => $model->lotStructures ? 'hidden' : 'btn btn-xs btn-info',
                            'role' => 'modal-remote',
                        ]); ?>
                        <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php Pjax::begin([
                                'id' => 'lot-info-view-container-pjax',
                                'enablePushState' => false
                            ]) ?>

                            <?php
                            try {
                                echo DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [
                                        'id',
                                        'name',
                                        'pos_num',
                                        'end_date:date',
                                        'auction_link:ntext',
                                        'auction_price',
                                        'rate',
                                        'max_auction_rate',
                                        'min_auction_rate',
                                        'sale_profit',
                                        'time_to_realization',
                                        'client_found',
                                        'lot_location',
                                        'opponent_auction_form_num',
                                        'form_is_filing',
                                        'deposit_is_payed',
                                        'tbankrot_link:ntext',
                                        'debtor_info:ntext',
                                        'stages:ntext',
                                        'is_archive',
                                        [
                                            'attribute' => 'lot_status_id',
                                            'value' => $model->lotStatus ? $model->lotStatus->name : null,
                                        ],
                                        [
                                            'attribute' => 'lot_group_id',
                                            'value' => $model->lotGroup ? $model->lotGroup->name : null,
                                        ],
                                        'competitive_manager_phone',
                                        'competitive_manager_email:email',
                                        [
                                            'attribute' => 'company_id',
                                            'value' => $model->company ? $model->company->name : null,
                                        ],
                                    ],
                                ]);
                            } catch (Exception $e) {
                                Yii::error($e->getMessage(), '_error');
                                echo $e->getMessage();
                            } ?>

                            <?php Pjax::end(); ?>
                        </div>
                        <div class="col-md-12">
                            <?= $model->description ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Правый блок-->
        <div class="col-md-7">
            <!-- Задачи-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Задачи
                            </h4>

                            <div class="panel-heading-btn" style="margin-top: -20px;">
                                <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning"
                                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php Pjax::begin([
                                        'id' => 'lot-task-view-container-pjax',
                                        'enablePushState' => false
                                    ]) ?>
                                    <?= $this->render('@app/views/lot-task/index', [
                                        'dataProvider' => $lotTaskDataProvider,
                                        'model' => $lot_task_model,
                                    ]) ?>
                                    <?php Pjax::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- Документы-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Документы
                            </h4>

                            <div class="panel-heading-btn" style="margin-top: -20px;">
                                <?= Html::a('<i class="fa fa-plus"></i>', ['/lot-document/create'], [
                                    'class' => 'btn btn-xs btn-icon btn-info',
                                    'role' => 'modal-remote'
                                ]) ?>
                                <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning"
                                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <!--               Задачи-->
                                <div class="col-md-12">
                                    <?php Pjax::begin([
                                        'id' => 'lot-task-view-container-pjax',
                                        'enablePushState' => false
                                    ]) ?>

                                    <p>Тут будут документы</p>
                                    <?php Pjax::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Состав лота-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-inverse panel-lot-structure" style="display: inline-block;">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Состав лота
                            </h4>
                            <div class="panel-heading-btn" style="margin-top: -20px;">
                                <?= Html::a('<i class="fa fa-refresh"></i>', ['lot/view', 'id' => $model->id], [
                                    'class' => 'btn btn-xs btn-icon btn-info',
                                ]) ?>
                                <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning"
                                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="group-info">
                                <?php if ($groups_info = $structure_model->groups_info): ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <ul>
                                                <?php foreach ($groups_info as $group_name => $info): ?>
                                                    <li>
                                                        <p><b><?= $group_name ?></b> -
                                                            Кол-во: <?= $info['count'] .
                                                            ' / Мин.: ' . $info['total_min'] .
                                                            ' / Макс.: ' . $info['total_max'] .
                                                            ' / Средн.: ' . ($info['total_min'] + $info['total_max']) / 2 ?>
                                                        </p>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <?php $form = ActiveForm::begin() ?>
                            <?php
                            echo $form->field($structure_model, 'structure')->widget(MultipleInput::className(), [
                                'min' => 1,
                                'addButtonOptions' => [
                                    'class' => 'multiple-input-list__btn js-input-plus btn btn-sm btn-default'
                                ],
                                'removeButtonOptions' => [
                                    'class' => 'multiple-input-list__btn js-input-remove btn btn-danger btn-sm'
                                ],
                                'columns' => [
                                    [
                                        'name' => 'id',
                                        'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                                        'options' => [
                                            'class' => 'ls-id',
                                            'style' => 'font-size: 10px;'
                                        ]

                                    ],
                                    [
                                        'name' => 'item_name',
                                        'title' => 'Название',
                                        'options' => [
                                            'onchange' => $onchange,
                                            'class' => 'item-name input-sm',
                                            'data-name' => 'item_name',
                                            'style' => 'font-size: 10px; min-width: 200px; padding: 0 2px',
                                        ],
                                    ],
                                    [
                                        'name' => 'group_id',
                                        'type' => 'dropDownList',
                                        'title' => 'Группа',
                                        'items' => $structure_model->getStructureGroupList(),
                                        'options' => [
                                            'onchange' => $onchange,
                                            'prompt' => 'Выберите группу',
                                            'data-name' => 'group_id',
                                            'class' => 'input-sm',
                                            'style' => 'font-size: 10px;'
                                        ]
                                    ],
                                    [
                                        'name' => 'count',
                                        'options' => [
                                            'onchange' => $onchange,
                                            'class' => 'count-item input-sm',
                                            'data-name' => 'count',
                                            'style' => 'font-size: 10px; min-width: 40px'
                                        ],
                                        'title' => 'Кол. (' . $structure_model->total_num . ')',
                                    ],
                                    [
                                        'name' => 'min_price',
                                        'options' => [
                                            'onchange' => $onchange,
                                            'class' => 'min-price input-sm',
                                            'data-name' => 'min_price',
                                            'style' => 'font-size: 10px; width: 60px'
                                        ],
                                        'title' => 'Мин. (' . $structure_model->total_sum_min . ')',
                                    ],
                                    [
                                        'name' => 'max_price',
                                        'options' => [
                                            'onchange' => $onchange,
                                            'class' => 'max-price input-sm',
                                            'data-name' => 'max_price',
                                            'style' => 'font-size: 10px; width: 60px'
                                        ],
                                        'title' => 'Макс. (' . $structure_model->total_sum_max . ')',
                                    ],
                                    [
                                        'name' => 'calculated_field',
                                        'type' => MultipleInputColumn::TYPE_STATIC,
                                        'title' => ($structure_model->total_sum_min + $structure_model->total_sum_max) / 2,
                                        //'Мин/Макс/Средн',
                                        'value' => function ($model) {
                                            if (!$model) {
                                                return '';
                                            }
                                            $min_sum = $model->min_price * $model->count;
                                            $max_sum = $model->max_price * $model->count;
                                            $mid_sum = ($min_sum + $max_sum) / 2;

                                            return $min_sum . '/' . $max_sum . '/' . $mid_sum;
                                        },
                                        'options' => [
                                            'class' => 'calculated-field',
                                            'style' => 'font-size: 10px;'
                                        ],
                                    ],
                                    [
                                        'name' => 'comment',
                                        'title' => 'Комментарий',
                                        'options' => [
                                            'onchange' => $onchange,
                                            'data-name' => 'comment',
                                            'class' => 'input-sm',
                                            'style' => 'font-size: 10px;'
                                        ]
                                    ],

                                    [
                                        'name' => 'source_link',
                                        'title' => 'Ссылка',
                                        'options' => [
                                            'onchange' => $onchange,
                                            'data-name' => 'source_link',
                                            'class' => 'input-sm',
                                            'style' => 'font-size: 10px;'
                                        ]
                                    ],
                                ]
                            ])->label(false);
                            ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</div>