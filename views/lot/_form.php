<?php

use app\models\Company;
use app\models\LotGroup;
use app\models\LotStatus;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Lot */
/* @var $form yii\widgets\ActiveForm */

/** @var User $identity */
$identity = Yii::$app->user->identity;
?>

<div class="lot-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php if ($identity->isSuperAdmin()): ?>
        <?php
        try {
            echo $form->field($model, 'company_id')->widget(\kartik\select2\Select2::class, [
                'data' => (new Company())->getList(),
                'options' => [
                    'prompt' => 'Выберите компанию'
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        } ?>
    <?php else: ?>
        <?= $form->field($model, 'company_id')->hiddenInput()->label(false) ?>
    <?php endif; ?>

    <div class="row">
<!--Левая колонка-->
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">

                    <?= $form->field($model, 'lot_status_id')->dropDownList((new LotStatus())->getList(),
                        [
                            'prompt' => 'Выберите статус'
                        ]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'lot_group_id')->dropDownList((new LotGroup())->getList(),
                        [
                            'prompt' => 'Выберите группу'
                        ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'description')->textarea(['rows' => 10]) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'debtor_info')->textarea(['rows' => 10]) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'stages')->textarea(['rows' => 10]) ?>
                </div>
            </div>
        </div>
<!--Правая колонка-->
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'pos_num')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'end_date')->input('date') ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'auction_link')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'auction_price')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'rate')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'max_auction_rate')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'min_auction_rate')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'sale_profit')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'time_to_realization')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'client_found')->checkbox() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'form_is_filing')->checkbox() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'deposit_is_payed')->checkbox() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'is_archive')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'lot_location')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'opponent_auction_form_num')->textInput() ?>
                </div>

                <div class="col-md-12">
                    <?= $form->field($model, 'tbankrot_link')->textInput() ?>
                </div>
                <div class="col-md-12">
                    <?php
                    try {
                        echo $form->field($model, 'access_users')->widget(\kartik\select2\Select2::class, [
                            'data' => ArrayHelper::map(User::getUsers()->all(), 'id', 'fio'),
                            'options' => [
                                'placeholder' => '...',
                                'multiple' => true,
                            ],
                            'pluginOptions' => [
                            ]
                        ]);
                    } catch (Exception $e) {
                        Yii::error($e->getMessage(), '_error');
                        echo $e->getMessage();
                    } ?>

                </div>


                <div class="col-md-12">
                    <?= $form->field($model, 'competitive_manager_phone')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'competitive_manager_email')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>











    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
