<?php

/* @var $this yii\web\View */
/* @var $model app\models\Lot */
?>
<div class="lot-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
