<?php

use app\widgets\LotView;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lots';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<?php Pjax::begin([
    'id' => 'lot-index-pjax',
    'enablePushState' => false
]) ?>
<?php
try {
    echo LotView::widget([
        'dataProvider' => $dataProvider,
    ]);
} catch (Exception $e) {
    Yii::error($e->getMessage(), '_error');
    echo $e->getMessage();
} ?>

<?php Pjax::end() ?>
<div class="lot-index">
    <!--    <div id="ajaxCrudDatatable">-->
    <!--        --><?php //echo GridView::widget([
    //            'id'=>'crud-datatable',
    //            'dataProvider' => $dataProvider,
    //            'filterModel' => $searchModel,
    //            'pjax'=>true,
    //            'columns' => require(__DIR__.'/_columns.php'),
    //            'toolbar'=> [
    //                ['content'=>
    //                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
    //                    ['role'=>'modal-remote','title'=> 'Create new Lots','class'=>'btn btn-default']).
    //                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
    //                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
    //                    '{toggleData}'.
    //                    '{export}'
    //                ],
    //            ],
    //            'striped' => true,
    //            'condensed' => true,
    //            'responsive' => true,
    //            'panel' => [
    //                'type' => 'primary',
    //                'heading' => '<i class="glyphicon glyphicon-list"></i> Lots listing',
    //                'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
    //                'after'=>BulkButtonWidget::widget([
    //                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete All',
    //                                ["bulk-delete"] ,
    //                                [
    //                                    "class"=>"btn btn-danger btn-xs",
    //                                    'role'=>'modal-remote-bulk',
    //                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    //                                    'data-request-method'=>'post',
    //                                    'data-confirm-title'=>'Are you sure?',
    //                                    'data-confirm-message'=>'Are you sure want to delete this item'
    //                                ]),
    //                        ]).
    //                        '<div class="clearfix"></div>',
    //            ]
    //        ])?>
    <!--    </div>-->
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
