<?php

use app\models\Lot;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
        'content' => function (Lot $model) {
            return \yii\helpers\Html::a($model->name, ['/lot/view', 'id' => $model->id]);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'lot_status_id',
        'filter' => (new \app\models\LotStatus())->getList(),
        'content' => function (Lot $model) {
            return $model->lotStatus ? $model->lotStatus->name : null;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'filter' => (new \app\models\LotGroup())->getList(),
        'attribute' => 'lot_group_id',
        'content' => function (Lot $model) {
            return $model->lotGroup ? $model->lotGroup->name : null;
        }
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],

//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'description',
//        'content' => function (Lot $model){
//            return substr($model->description, 0, 200);
//        }
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'pos_num',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'end_date',
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'auction_link',
//    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'auction_price',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'rate',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'max_auction_rate',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'min_auction_rate',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'sale_profit',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'time_to_realization',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'client_found',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'lot_location',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'opponent_auction_form_num',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'form_is_filing',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'deposit_is_payed',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'tbankrot_link',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'debtor_info',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'stages',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'is_archive',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'access_users',
        'content' => function (Lot $model) {
            return $model->getAccessUsersList(false);
        }
    ],

    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'competitive_manager_phone',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'competitive_manager_email',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'company_id',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'
        ],
    ],

];   