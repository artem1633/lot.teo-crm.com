<?php

use app\admintheme\widgets\Menu;

/** @var \app\models\User $identity */
$identity = Yii::$app->user->identity;
?>

<div id="sidebar" class="sidebar">
    <?php if (Yii::$app->user->isGuest == false): ?>
        <?php
        try {
            echo Menu::widget(
                [
                    'encodeLabels' => false,
                    'options' => ['class' => 'nav'],
                    'items' => [
//                        [
//                            'label' => 'Лоты',
//                            'icon' => 'fa fa-gavel',
//                            'url' => '#',
//                            'options' => ['class' => 'has-sub'],
//                            'items' => [
//                                ['label' => 'Задачи', 'icon' => 'fa fa-tasks', 'url' => ['/lot-task']],
//                                ['label' => 'Информация', 'icon' => 'fa fa-info', 'url' => ['/lot'],],
//                            ],
//                        ],
                        [
                            'label' => 'Лоты',
                            'icon' => 'fa fa-gavel',
                            'url' => '/lot',
                        ],
                        [
                            'label' => 'Справочники',
                            'icon' => 'fa fa-book',
                            'url' => '#',
                            'options' => ['class' => 'has-sub'],
                            'items' => [
                                [
                                    'label' => 'Компании',
                                    'icon' => 'fa fa-bank',
                                    'url' => ['/company'],
                                    'visible' => $identity->isSuperAdmin()
                                ],
                                [
                                    'label' => 'Пользователи',
                                    'icon' => 'fa fa-user-o',
                                    'url' => ['/user'],
                                    'visible' => $identity->isAdmins()
                                ],
                                [
                                    'label' => 'Статусы лотов',
                                    'icon' => 'fa fa-calendar-check-o',
                                    'url' => ['/lot-status'],
                                    'visible' => $identity->isAdmins(),
                                ],
                                [
                                    'label' => 'Группы лотов',
                                    'icon' => 'fa fa-users',
                                    'url' => ['/lot-group'],
                                    'visible' => $identity->isAdmins(),
                                ],
                                [
                                    'label' => 'Группы состава лота',
                                    'icon' => 'fa fa-users',
                                    'url' => ['/lot-structure-group'],
                                    'visible' => $identity->isAdmins(),
                                ],
                            ],
                        ],
                        ['label' => 'Обратная связь', 'url' => ['/ticket'], 'icon' => 'fa fa-question'],
                    ],
                ]
            );
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'error');
            echo $e->getMessage();
        }
        ?>
    <?php endif; ?>
</div>
