<?php

namespace app\models;

use app\models\query\LotQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "lot".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $description Описание
 * @property int $pos_num Кол-во позиций
 * @property string $end_date дата окончания этапа/торгов
 * @property string $auction_link Ссылка на торги/ЭТП
 * @property double $auction_price Цена на торгах
 * @property double $rate Наша ставка
 * @property double $max_auction_rate Максимальная цена рынка
 * @property double $min_auction_rate Минимальная цена рынка
 * @property double $sale_profit Прибыль с продажи
 * @property int $time_to_realization Время на реаизацию
 * @property int $client_found Клиент найден
 * @property string $lot_location Местонахождение лота
 * @property int $opponent_auction_form_num Кол-во заявок оппонентов
 * @property int $form_is_filing Заявка подана
 * @property int $deposit_is_payed Задаток оплачен
 * @property string $tbankrot_link Ссылка на tbankrot.ru
 * @property string $debtor_info Информация о должнике
 * @property string $stages Этапы
 * @property int $is_archive Архив
 * @property int $lot_status_id Статус
 * @property int $lot_group_id Группа
 * @property string $competitive_manager_phone Телефон конкурсного управлябщего
 * @property string $competitive_manager_email Email конкурсного управляющего
 * @property int $company_id Компания-создаель
 * @property array $access_users Пользователи, которым доступен лот
 *
 * @property LotStructure[] $lotStructures Структура лота
 * @property User[] $accessUsers Пользователи которым доступен лот
 * @property Company $company
 * @property LotGroup $lotGroup
 * @property LotStatus $lotStatus
 * @property LotTask[] $lotTasks
 * @property LotToUser[] $lotToUsers
 */
class Lot extends ActiveRecord
{
    public $access_users;
    public $structure;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'auction_link', 'tbankrot_link', 'debtor_info', 'stages'], 'string'],
            [
                [
                    'pos_num',
                    'time_to_realization',
                    'client_found',
                    'opponent_auction_form_num',
                    'form_is_filing',
                    'deposit_is_payed',
                    'is_archive',
                    'lot_status_id',
                    'lot_group_id',
                    'company_id'
                ],
                'integer'
            ],
            [['end_date'], 'safe'],
            [['auction_price', 'rate', 'max_auction_rate', 'min_auction_rate', 'sale_profit'], 'number'],
            [
                ['name', 'lot_location', 'competitive_manager_phone', 'competitive_manager_email'],
                'string',
                'max' => 255
            ],
            [
                ['company_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Company::className(),
                'targetAttribute' => ['company_id' => 'id']
            ],
            [
                ['lot_group_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => LotGroup::className(),
                'targetAttribute' => ['lot_group_id' => 'id']
            ],
            [
                ['lot_status_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => LotStatus::className(),
                'targetAttribute' => ['lot_status_id' => 'id']
            ],
            [['access_users', 'structure'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'description' => 'Описание',
            'pos_num' => 'Кол-во позиций',
            'end_date' => 'дата окончания этапа/торгов',
            'auction_link' => 'Ссылка на торги/ЭТП',
            'auction_price' => 'Цена на торгах',
            'rate' => 'Наша ставка',
            'max_auction_rate' => 'Максимальная цена рынка',
            'min_auction_rate' => 'Минимальная цена рынка',
            'sale_profit' => 'Прибыль с продажи',
            'time_to_realization' => 'Время на реаизацию',
            'client_found' => 'Клиент найден',
            'lot_location' => 'Местонахождение лота',
            'opponent_auction_form_num' => 'Кол-во заявок оппонентов',
            'form_is_filing' => 'Заявка подана',
            'deposit_is_payed' => 'Задаток оплачен',
            'tbankrot_link' => 'Ссылка на tbankrot.ru',
            'debtor_info' => 'Информация о должнике',
            'stages' => 'Этапы',
            'is_archive' => 'Архив',
            'lot_status_id' => 'Статус',
            'lot_group_id' => 'Группа',
            'competitive_manager_phone' => 'Телефон конкурсного управлябщего',
            'competitive_manager_email' => 'Email конкурсного управляющего',
            'company_id' => 'Компания-создаель',
            'access_users' => 'Доступен пользователям',
        ];
    }

    public function beforeSave($insert)
    {
        /** @var User $identity */
        $identity = \Yii::$app->user->identity;
        if (!$this->company_id) {
            $this->company_id = $identity->company_id;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \yii\db\Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->access_users) {
            LotToUser::deleteAll(['lot_id' => $this->id]);
            $rows = [];

            /** @var int $access_user_id */
            foreach ($this->access_users as $access_user_id) {
                $rows[] = [$this->id, $access_user_id];
            }
            LotToUser::deleteAll(['lot_id' => $this->id]);

            \Yii::$app->db->createCommand()->batchInsert(LotToUser::tableName(), ['lot_id', 'user_id'],
                $rows)->execute();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLotGroup()
    {
        return $this->hasOne(LotGroup::className(), ['id' => 'lot_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLotStatus()
    {
        return $this->hasOne(LotStatus::className(), ['id' => 'lot_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLotTasks()
    {
        return $this->hasMany(LotTask::className(), ['lot_id' => 'id']);
    }

    public function getLotStructures()
    {
        return $this->hasMany(LotStructure::class, ['lot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLotToUsers()
    {
        return $this->hasMany(LotToUser::className(), ['lot_id' => 'id']);
    }

    /**
     * Получает пользователей, у которых есть доступ к лоту
     * @return \yii\db\ActiveQuery
     */
    public function getAccessUsers()
    {
        return User::find()
            ->joinWith(['availableLots'])
            ->andWhere(['lot.id' => $this->id]);
    }

    /**
     * {@inheritdoc}
     * @return LotQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LotQuery(get_called_class());
    }

    /**
     * Список пользователей, имеющих доступ к лоту
     * @param bool $to_array True - возвращается в массиве id=>fio
     * False - возвращается строка, ФИО разделены запятой
     * @return array|string
     */
    public function getAccessUsersList($to_array = true)
    {
        $query = $users = User::find()
            ->joinWith(['availableLots'])
            ->andWhere(['lot.id' => $this->id]);

        if ($to_array) {
            return ArrayHelper::map($query->all(), 'id', 'fio');
        } else {
            $users = $query->select('user.fio')->column() ?? null;

            if ($users) {
                return $users ? implode(', ', $users) : '';
            }
            return null;
        }
    }

    public function getList($show_all = true)
    {
        /** @var User $identity */
        $identity = \Yii::$app->user->identity;

        if ($identity->isSuperAdmin()){
            $show_all = true;
        }

        if ($show_all) {
            $query = self::find();
        } else {
            $query = self::find()
                ->joinWith(['lotToUsers'])
                ->andWhere(['lot_to_user.user_id' => $identity->id]);
        }
        return ArrayHelper::map($query->all(), 'id', 'name');
    }
}
