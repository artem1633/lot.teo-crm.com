<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LotStructure;

/**
 * LotStructureSearch represents the model behind the search form about `app\models\LotStructure`.
 */
class LotStructureSearch extends LotStructure
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'count', 'group_id', 'lot_id'], 'integer'],
            [['item_name', 'comment', 'source_link'], 'safe'],
            [['min_price', 'max_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LotStructure::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'count' => $this->count,
            'min_price' => $this->min_price,
            'max_price' => $this->max_price,
            'group_id' => $this->group_id,
            'lot_id' => $this->lot_id,
        ]);

        $query->andFilterWhere(['like', 'item_name', $this->item_name])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'source_link', $this->source_link]);

        return $dataProvider;
    }
}
