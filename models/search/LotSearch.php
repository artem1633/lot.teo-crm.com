<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Lot;

/**
 * LotSearch represents the model behind the search form about `app\models\Lot`.
 */
class LotSearch extends Lot
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pos_num', 'time_to_realization', 'client_found', 'opponent_auction_form_num', 'form_is_filing', 'deposit_is_payed', 'is_archive', 'lot_status_id', 'lot_group_id', 'company_id'], 'integer'],
            [['name', 'description', 'end_date', 'auction_link', 'lot_location', 'tbankrot_link', 'debtor_info', 'stages', 'competitive_manager_phone', 'competitive_manager_email'], 'safe'],
            [['auction_price', 'rate', 'max_auction_rate', 'min_auction_rate', 'sale_profit'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Lot::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pos_num' => $this->pos_num,
            'end_date' => $this->end_date,
            'auction_price' => $this->auction_price,
            'rate' => $this->rate,
            'max_auction_rate' => $this->max_auction_rate,
            'min_auction_rate' => $this->min_auction_rate,
            'sale_profit' => $this->sale_profit,
            'time_to_realization' => $this->time_to_realization,
            'client_found' => $this->client_found,
            'opponent_auction_form_num' => $this->opponent_auction_form_num,
            'form_is_filing' => $this->form_is_filing,
            'deposit_is_payed' => $this->deposit_is_payed,
            'is_archive' => $this->is_archive,
            'lot_status_id' => $this->lot_status_id,
            'lot_group_id' => $this->lot_group_id,
            'company_id' => $this->company_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'auction_link', $this->auction_link])
            ->andFilterWhere(['like', 'lot_location', $this->lot_location])
            ->andFilterWhere(['like', 'tbankrot_link', $this->tbankrot_link])
            ->andFilterWhere(['like', 'debtor_info', $this->debtor_info])
            ->andFilterWhere(['like', 'stages', $this->stages])
            ->andFilterWhere(['like', 'competitive_manager_phone', $this->competitive_manager_phone])
            ->andFilterWhere(['like', 'competitive_manager_email', $this->competitive_manager_email]);

        return $dataProvider;
    }
}
