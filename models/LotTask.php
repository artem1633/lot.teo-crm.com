<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "lot_task".
 *
 * @property int $id
 * @property string $text
 * @property int $done
 * @property int $lot_id
 *
 * @property Lot $lot
 */
class LotTask extends ActiveRecord
{

    public $company;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lot_task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['done', 'lot_id'], 'integer'],
            [['text'], 'string', 'max' => 255],
            [['lot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lot::className(), 'targetAttribute' => ['lot_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'done' => 'Завершена',
            'lot_id' => 'Лот',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'lot_id']);
    }
}
