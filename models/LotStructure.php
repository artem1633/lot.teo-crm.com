<?php

namespace app\models;

use app\models\query\LotStructureQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "lot_structure".
 *
 * @property int $id
 * @property string $item_name Название
 * @property int $count Количество
 * @property double $min_price Минимальная стоимость
 * @property double $max_price Максимальная стоимость
 * @property string $comment Комментарий
 * @property int $group_id Группа
 * @property string $source_link Ссылка на источник
 * @property int $lot_id Лот
 * @property int $total_num Общее кол-во
 * @property int $total_sum_min Общее минимальная сумма
 * @property int $total_sum_max Общее максимальная сумма
 * @property array $groups_info Стоимость по группамм
 *
 * @property LotStructureGroup $group
 * @property Lot $lot
 * @property LotStructure[] $structure
 */
class LotStructure extends ActiveRecord
{
    public $structure;
    public $calculated_field;
    public $total_num;
    public $total_sum_min;
    public $total_sum_max;

    /** @var array */
    public $groups_info;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lot_structure';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['count', 'group_id', 'lot_id'], 'integer'],
            [['min_price', 'max_price'], 'number'],
            [['comment', 'source_link'], 'string'],
            [['item_name'], 'string', 'max' => 255],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => LotStructureGroup::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['lot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lot::className(), 'targetAttribute' => ['lot_id' => 'id']],
            ['structure', 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_name' => 'Название',
            'count' => 'Количество',
            'min_price' => 'Минимальная стоимость',
            'max_price' => 'Максимальная стоимость',
            'comment' => 'Комментарий',
            'group_id' => 'Группа',
            'source_link' => 'Ссылка на источник',
            'lot_id' => 'Лот',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(LotStructureGroup::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'lot_id']);
    }

    /**
     * {@inheritdoc}
     * @return LotStructureQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LotStructureQuery(get_called_class());
    }

    /**
     * Группы структуры
     * @return array
     */
    public function getStructureGroupList()
    {
        $query = LotStructureGroup::find()->forCurrentCompany();
        return ArrayHelper::map($query->all(), 'id', 'name');
    }

    public function setTotals()
    {
        /** @var User $identity */
        $identity = \Yii::$app->user->identity;

        \Yii::info($this->attributes, 'test');
        $this->total_num = LotStructure::find()->forLot($this->lot_id)->sum('count');
        $this->total_sum_min = LotStructure::find()->forLot($this->lot_id)->sum('min_price');
        $this->total_sum_max = LotStructure::find()->forLot($this->lot_id)->sum('max_price');

        $groups = LotStructureGroup::find()->forCompany($identity->company_id)->all();

        if (!$groups) return false;

        /** @var LotStructureGroup $group */
        foreach ($groups as $group) {
            $count = LotStructure::find()->forLot($this->lot_id)->andWhere(['group_id' => $group->id])->sum('count');
            if (!$count) continue;
            $this->groups_info[$group->name]['count'] =  $count;
            $this->groups_info[$group->name]['total_min'] =  LotStructure::find()->forLot($this->lot_id)->andWhere(['group_id' => $group->id])->sum('min_price');
            $this->groups_info[$group->name]['total_max'] =  LotStructure::find()->forLot($this->lot_id)->andWhere(['group_id' => $group->id])->sum('max_price');
        }

        return true;
    }



}
