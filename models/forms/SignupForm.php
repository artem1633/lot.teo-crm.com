<?php
namespace app\models\forms;

use app\models\Company;
use yii\base\Model;
use app\models\User;

/**
 * Signup form
 *
 * @property string $fio Ф.И.О.
 * @property string $phone
 * @property string $email
 * @property string $password
 * @property int is_company_super_admin
 */
class SignupForm extends Model
{
    public $fio;
    public $phone;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'email', 'password'], 'required'],
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'targetAttribute' => 'email', 'message' => 'Этот email уже зарегистрирован',],
//            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Этот email уже зарегистрирован',
//                'when' => function($model, $attribute){
//                    if($this->userId != null)
//                    {
//                        $userModel = User::findOne($this->userId);
//                        return $this->{$attribute} !== $userModel->getOldAttribute($attribute);
//                    }
//                    return true;
//                },
//            ],
            ['password', 'string', 'min' => 6],
        ];
    }

    // public function scenarios()
    // {
    //     $scenarios = parent::scenarios();
    //     $scenarios['update'] = ['password', 'email'];//Scenario Values Only Accepted
    //     return $scenarios;
    // }

    public function attributeLabels()
    {
        return [
            'fio' => 'Ф.И.О.',
            'phone' => 'Телефон',
            'email' => 'Email',
            'password' => 'Пароль',
        ];
    }

    /**
     * Signs user up.
     *
     * @return true
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $company = new Company([
            'name' => $this->fio,
            'phone' => $this->phone,
            'email' => $this->email,
        ]);
        $companySave = $company->save(false);


        $user = new User();
        $user->detachBehavior('company');
        $user->fio = $this->fio;
        $user->role = User::ROLE_COMPANY_ADMIN;
        $user->setPassword($this->password);
        $user->company_id = $company->id;
        $user->email = $this->email;
        $user->phone = $this->phone;
        $user->is_company_super_admin = 1;

        $userSave = $user->save(false);

        return ($companySave && $userSave);
    }

    /**
     * update user.
     *
     * @param User $user
     * @return User|null the saved model or null if saving fails
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function update($user)
    {
        if (!$this->validate()) {
            return null;
        }

        $user->fio = $this->fio;
        $user->phone = $this->phone;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->password = $this->password;

        if ($user->update()){
            return $user;
        } else {
            return null;
        }
    }
}
