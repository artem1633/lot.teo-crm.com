<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "lot_group".
 *
 * @property int $id
 * @property string $name
 * @property string $color
 * @property int $company_id
 *
 * @property Company $company
 */
class LotGroup extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lot_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'color'], 'string', 'max' => 255],
            ['company_id', 'integer'],
            ['company_id', 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'color' => 'Цвет',
            'company_id' => 'Компания',
        ];
    }

    /**
     * Возвращает список групп
     * @return array
     */
    public function getList()
    {
        return ArrayHelper::map(self::find()->andWhere(['company_id' => \Yii::$app->user->identity->company_id])->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }
}

