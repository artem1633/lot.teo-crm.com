<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $address Фактический адрес
 * @property int $is_super_company Является ли супер компанией
 * @property int $access Доступ (вкл/выкл)
 * @property string $last_activity_datetime Дата и время последней активности
 * @property string $created_at
 * @property string $email Email
 * @property string $phone Телефон
 * @property string api_key АПИ ключ
 *
 * @property CompanyFile[] $files
 * @property User[] $users
 * @property Lot[] $lots
 */
class Company extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_super_company', 'access'], 'integer'],
            [
                [
                    'last_activity_datetime',
                    'created_at',
                ],
                'safe'
            ],
            [
                [
                    'name',
                    'address',
                    'email',
                    'phone',
                ],
                'string',
                'max' => 255
            ],
            ['api_key', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'address' => 'Адрес',
            'is_super_company' => 'Является ли супер компанией',
            'access' => 'Доступ (вкл/выкл)',
            'last_activity_datetime' => 'Дата и время последней активности',
            'created_at' => 'Дата и время создания',
            'email' => 'Email',
            'phone' => 'Телефон',
            'api_key' => 'АПИ ключ',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            foreach (CompanyFile::typeList() as $type) {
                (new CompanyFile(['company_id' => $this->id, 'type' => $type]))->save(false);
            }
        }
    }

    /**
     * Отправляет письмо
     * @param $subject
     * @param $htmlBody
     * @param array $attachments
     * @return bool
     */
    public function sendEmailMessage($subject, $htmlBody, $attachments = [])
    {
        if ($this->email == null) {
            return false;
        }

        try {
            $mail = Yii::$app->mailer->compose()
                ->setFrom('debtprice@yandex.ru')
                ->setTo($this->email)
                ->setSubject($subject)
                ->setHtmlBody($htmlBody);

            if (count($attachments) > 0) {
                foreach ($attachments as $attachmentPath => $attachmentName) {
                    $mail->attach($attachmentPath, ['fileName' => $attachmentName]);
                }
            }

            $mail->send();
        } catch (\Exception $e) {
            Yii::warning($e);
            return false;
        }
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(CompanyFile::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['company_id' => 'id']);
    }

    /**
     * Получает компанию по токену
     * @param $token
     * @return null|static
     */
    public function getByToken($token)
    {
        return self::findOne(['api_key' => $token]) ?? null;
    }

    /**
     * Генерация нового токена
     * @return string
     * @throws \yii\base\Exception
     */
    public function changeApiKey()
    {
        $this->api_key = Yii::$app->security->generateRandomString();

        $this->save(false);

        return $this->api_key;
    }

    public function getList()
    {
        return ArrayHelper::map(self::find()->andWhere(['<>', 'is_super_company', 1])->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLots()
    {
        return $this->hasMany(Lot::class, ['company_id' => 'id']);
    }
}
