<?php

namespace app\models;

use app\models\query\LotStructureGroupQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "lot_structure_group".
 *
 * @property int $id
 * @property string $name
 * @property int $company_id
 *
 * @property Company $company
 * @property LotStructure[] $lotStructures
 */
class LotStructureGroup extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lot_structure_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            ['company_id', 'integer'],
            ['company_id', 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'company_id' => 'Компания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLotStructures()
    {
        return $this->hasMany(LotStructure::className(), ['group_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return LotStructureGroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LotStructureGroupQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }
}
