<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\LotStructure]].
 *
 * @see \app\models\LotStructure
 */
class LotStructureQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    public function forLot($id)
    {
        return $this->andWhere(['lot_id' => $id]);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\LotStructure[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\LotStructure|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
