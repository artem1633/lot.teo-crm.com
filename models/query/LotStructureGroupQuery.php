<?php

namespace app\models\query;

use Yii;

/**
 * This is the ActiveQuery class for [[\app\models\LotStructureGroup]].
 *
 * @see \app\models\LotStructureGroup
 */
class LotStructureGroupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    public function forCompany($id)
    {
        return $this->andWhere(['company_id' => $id]);
    }

    public function forCurrentCompany()
    {
        $id = Yii::$app->user->identity->company_id;
        return $this->andWhere(['company_id' => $id]);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\LotStructureGroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\LotStructureGroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
