<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "lot_to_user".
 *
 * @property int $id
 * @property int $lot_id Лот
 * @property int $user_id Пользователь
 */
class LotToUser extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lot_to_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lot_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lot_id' => 'Лот',
            'user_id' => 'Пользователь',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\LotToUserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\LotToUserQuery(get_called_class());
    }

    /**
     * Возвращает массив с идентификаторами пользователей
     * @param int $id Идентификатор лота
     * @return array
     */
    public function getUsersForLot($id)
    {
        return self::find()->andWhere(['lot_id' => $id])->select('user_id')->column();
    }
}
