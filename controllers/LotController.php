<?php

namespace app\controllers;

use app\models\LotStructure;
use app\models\LotTask;
use app\models\LotToUser;
use app\models\search\LotTaskSearch;
use app\models\User;
use Yii;
use app\models\Lot;
use app\models\search\LotSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * LotController implements the CRUD actions for Lot model.
 */
class LotController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Lot models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LotSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['id' => SORT_DESC]);

        /** @var User $identity */
        $identity = Yii::$app->user->identity;
        if (!$identity->isSuperAdmin()) {
            $dataProvider->query->andWhere(['company_id' => $identity->company_id]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Lot model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $task_model = new LotTask();
        $task_model->lot_id = $model->id;
        $task_model->done = 0;

        $lotTaskSearchModel = new LotTaskSearch();
        $lotTaskProvider = $lotTaskSearchModel->search(['lot_id' => $model->id]);
        $lotTaskProvider->query->orderBy(['done' => SORT_ASC]);
        $lotTaskProvider->query->andWhere(['lot_id' => $model->id]);

        $structure_model = new LotStructure();
        $structure_model->lot_id = $model->id;
        $structure_model->structure = LotStructure::find()->forLot($model->id)->all();
        $structure_model->setTotals();

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Лот #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                    'taskModel' => $task_model,
                    'lotTaskSearchModel' => $lotTaskSearchModel,
                    'lotTaskProvider' => $lotTaskProvider,
                    'structure_model' => $structure_model,
                ]),
                'footer' => Html::button('Закрыть',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Редактировать', ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $model,
                'lot_task_model' => $task_model,
                'lotTaskSearchModel' => $lotTaskSearchModel,
                'lotTaskDataProvider' => $lotTaskProvider,
                'structure_model' => $structure_model,
            ]);
        }
    }

    /**
     * Creates a new Lot model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Lot();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создание лота",
                    'size' => 'large',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return [
                        'forceReload' => '#lot-index-pjax',
                        'forceClose' => true,
                    ];
                } else {
                    return [
                        'title' => "Создание лота",
                        'size' => 'large',
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Lot model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $model->access_users = (new LotToUser())->getUsersForLot($model->id);

        Yii::info($model->access_users, 'test');

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактирование лота #" . $id,
                    'size' => 'large',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return [
                        'forceReload' => '#lot-index-pjax',
                        'forceClose' => true,
                    ];
                } else {
                    return [
                        'title' => "Редактирование лота #" . $id,
                        'size' => 'large',
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Lot model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Lot model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Lot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lot::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Отображает форму состава лота
     * @param int $id Идентификатор лота
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionStructure($id)
    {
        $lot = Lot::findOne($id);

        if (!LotStructure::find()->andWhere(['lot_id' => $lot->id])->exists()) {
            $lot_text = $lot->description;

            $lot_text = strip_tags($lot_text);

            $lot_items = explode(PHP_EOL, $lot_text);
            $rows = [];
            //Сохраняем в структуру лота
            foreach ($lot_items as $item) {
                if ($item) {
                    $rows[] = [$item, $lot->id];
                }
            }
            Yii::$app->db->createCommand()->batchInsert(LotStructure::tableName(), ['item_name', 'lot_id'],
                $rows)->execute();
        }
//        return $this->redirect(['view', 'id' => $lot->id]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'forceReload' => '#lot-task-view-container-pjax',
            'label' => 'Формирование списка лота',
            'content' => 'Список сформирован успешно'
        ];

//        $structure_model = new LotStructure();
//
//        $structure_items = LotStructure::find()->andWhere(['lot_id' => $lot->id])->all();
//        $structure_model->structure = $structure_items;
//
//        $searchModel = new LotStructureSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        $dataProvider->query->andWhere(['lot_id' => $lot->id]);
//
//        return $this->render('@app/views/lot-structure/index',
//                [
//                    'searchModel' => $searchModel,
//                    'dataProvider' => $dataProvider,
//                ]);
    }
}
