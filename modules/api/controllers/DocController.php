<?php

namespace app\modules\api\controllers;

use yii\base\Controller;
use yii\filters\AccessControl;

/**
 * Default controller for the `api` module
 */
class DocController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('view');
    }

}
