<?php

namespace app\modules\api\controllers;

use app\models\Company;
use app\models\Currency;
use app\models\ProductType;
use yii\web\Controller;
use yii\web\Response;

/**
 * Loan controller for the `api` module
 */
class LoanController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }


    /**
     * Получает все типы кредитных ппродуктов
     * @param string $token Токен компании
     * @return array
     */
    public function actionTypes($token)
    {
        $company = (new Company())->getByToken($token);

        if (!$company) {
            return ['success' => 0, 'error' => 'Не верный токен доступа'];
        }
        return ['success' => 1, 'data' => (new ProductType())->getList()];

    }

    /**
     * Получает все валюты
     * @param string $token Токен компании
     * @return array
     */
    public function actionCurrencies($token)
    {
        $company = (new Company())->getByToken($token);

        if (!$company) {
            return ['success' => 0, 'error' => 'Не верный токен доступа'];
        }
        return ['success' => 1, 'data' => (new Currency())->getList()];
    }


}
