<h1>Описание АПИ</h1>
<hr>
<h2>Доступ</h2>
<p>Для успешного вызова методов API необходим токен доступа компании</p>
<p>&nbsp;</p>
<h2><b>Добавление должника и дела</b></h2>
<h3>Запрос</h3>
<p>Адрес: <?= Yii::$app->request->hostName ?>/api/debtor/add</p>
<p>Метод: POST</p>
<div>
    <table align="left" border="1" cellpadding="5" cellspacing="0" class="table">
        <thead>
        <tr>
            <th scope="col">Параметр</th>
            <th scope="col">Тип</th>
            <th scope="col">Описание</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>token</td>
            <td>string</td>
            <td>Токен доступа компании</td>
        </tr>
        <tr>
            <td>debtor</td>
            <td>array</td>
            <td>Массив данных по должнику</td>
        </tr>
        <tr>
            <td>debtor.outer_id</td>
            <td>string</td>
            <td>&nbsp;Идентификатор из ПО кредитора</td>
        </tr>
        <tr>
            <td>debtor.age</td>
            <td>int</td>
            <td>&nbsp;Возраст</td>
        </tr>
        <tr>
            <td>debtor.gender</td>
            <td>int</td>
            <td>&nbsp;Пол. Мужской - 1, Женксий - 2</td>
        </tr>
        <tr>
            <td>debtor.marital_status</td>
            <td>int</td>
            <td>&nbsp;Семейное положение. Не состоит в браке - 0, Состоит в браке - 1</td>
        </tr>
        <tr>
            <td>debtor.birthday_date</td>
            <td>string</td>
            <td>&nbsp;Дата рождения в формате ГГГГ-ММ-ДД</td>
        </tr>
        <tr>
            <td>debtor.birthplace</td>
            <td>string</td>
            <td>&nbsp;Место рождения</td>
        </tr>
        <tr>
            <td>debtor.education</td>
            <td>string</td>
            <td>&nbsp;Образование</td>
        </tr>
        <tr>
            <td>debtor.email</td>
            <td>string</td>
            <td>&nbsp;Адрес электронной почты</td>
        </tr>
        <tr>
            <td>debtor.number_minor_children</td>
            <td>int</td>
            <td>&nbsp;Кол-во несовершеннолетних детей</td>
        </tr>
        <tr>
            <td>debtor.owner_property</td>
            <td>string</td>
            <td>Наличие в собственности имущества</td>
        </tr>
        <tr>
            <td>debtor.fact_residence_postcode</td>
            <td>string</td>
            <td>Индекс фактического проживания</td>
        </tr>
        <tr>
            <td>debtor.fact_residence_region</td>
            <td>string</td>
            <td>Регион фактического проживания</td>
        </tr>
        <tr>
            <td>debtor.fact_residence_city</td>
            <td>string</td>
            <td>Город фактического проживания</td>
        </tr>
        <tr>
            <td>debtor.fact_residence_address</td>
            <td>string</td>
            <td>Адрес фактического проживания</td>
        </tr>
        <tr>
            <td>debtor.register_region</td>
            <td>string</td>
            <td>Регион постоянной регистрации</td>
        </tr>
        <tr>
            <td>debtor.register_city</td>
            <td>string</td>
            <td>Город постоянной регистрации</td>
        </tr>
        <tr>
            <td>debtor.register_address</td>
            <td>string</td>
            <td>Адрес постоянной регистрации</td>
        </tr>
        <tr>
            <td>debtor.status</td>
            <td>int</td>
            <td>Статус (Список статусов можно получить запросом /api/debtor/statuses)</td>
        </tr>
        <tr>
            <td>debtor.workplace</td>
            <td>string</td>
            <td>Место работы</td>
        </tr>
        <tr>
            <td>debtor.position</td>
            <td>string</td>
            <td>Должность</td>
        </tr>
        <tr>
            <td>debtor.employer_city</td>
            <td>string</td>
            <td>Город работодателя</td>
        </tr>
        <tr>
            <td>debtor.employer_address</td>
            <td>string</td>
            <td>Адрес работодателя</td>
        </tr>
        <tr>
            <td>debtor.period_last_job</td>
            <td>int</td>
            <td>Срок работы на последнем рабочем месте (лет)&nbsp;</td>
        </tr>
        <tr>
            <td>loan</td>
            <td>array</td>
            <td>Массив данных по кредиту (делу)</td>
        </tr>
        <tr>
            <td>loan.date</td>
            <td>string</td>
            <td>Дата пердоставления кредита (в формате ГГГГ-ММ-ДД)</td>
        </tr>
        <tr>
            <td>loan.type</td>
            <td>int</td>
            <td>Тип кредитного продукта (Список кредитных&nbsp;продуктов&nbsp;можно получить запросом /api/loan/types)</td>
        </tr>
        <tr>
            <td>loan.currency_id</td>
            <td>int</td>
            <td>Валюта (Список валют можно получить запросом /api/loan/currencies</td>
        </tr>
        <tr>
            <td>loan.name_collateral</td>
            <td>string</td>
            <td>Наименование залога</td>
        </tr>
        <tr>
            <td>loan.assessment_collateral</td>
            <td>string</td>
            <td>Оценка залога</td>
        </tr>
        <tr>
            <td>loan.primary_amount_debt</td>
            <td>double</td>
            <td>Сумма задолженности по основному долгу (в валюте долга)</td>
        </tr>
        <tr>
            <td>loan.percent_amount_debt</td>
            <td>double</td>
            <td>Сумма задолженности по процентам (в валюте долга)</td>
        </tr>
        <tr>
            <td>loan.other_amount_debt</td>
            <td>double</td>
            <td>Сумма задолженности по иным платам и штрафам (в валюте долга)</td>
        </tr>
        <tr>
            <td>loan.total_amount_debt</td>
            <td>double</td>
            <td>Общая сумма задолженности (в валюте долга)</td>
        </tr>
        <tr>
            <td>loan.period</td>
            <td>int</td>
            <td>Срок кредита (дни)</td>
        </tr>
        <tr>
            <td>loan.expired_days</td>
            <td>int</td>
            <td>Кол-во дней просрочки</td>
        </tr>
        <tr>
            <td>loan.amount_last_payment</td>
            <td>double</td>
            <td>Сумма последнего платежа</td>
        </tr>
        <tr>
            <td>loan.total_payment</td>
            <td>double</td>
            <td>Общая сумма поступившая в счет оплаты по кредиту</td>
        </tr>
        <tr>
            <td>loan.last_payment_date</td>
            <td>string</td>
            <td>Дата последнего платежа (в формате ГГГГ-ММ-ДД)</td>
        </tr>
        <tr>
            <td>loan.last_contact_date</td>
            <td>string</td>
            <td>Дата последнего контакта с должником (в формате ГГГГ-ММ-ДД)</td>
        </tr>
        <tr>
            <td>loan.total_number_payment</td>
            <td>int</td>
            <td>Число произведенных платежей</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="example">
    <h4>Пример запроса PHP:</h4>
    <p>$url =&#39;<?= Yii::$app->request->hostName ?>/api/debtor/add&#39;;</p>
    <div>&nbsp;$data = [</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &#39;token&#39; =&gt; &lt;Токен доступа&gt;,</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &#39;debtor&#39; =&gt; [</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &#39;outer_id&#39; =&gt; &#39;56465465fdsf&#39;,</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &#39;age&#39; =&gt; 35,</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &#39;gender&#39; =&gt; 1,</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;...</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ],</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &#39;loan&#39; =&gt; [</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &#39;date&#39; =&gt; &#39;2015-02-02&#39;,</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &#39;type&#39; =&gt; 2,</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &#39;currency_id&#39; =&gt; 1,</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &#39;name_collateral&#39; =&gt; &#39;&#39;,</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ...</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ]</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; ];</div>
    <div>&nbsp;</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; $json = json_encode($data);</div>
    <div>&nbsp;</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; if ($curl = curl_init()) {</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; curl_setopt($curl, CURLOPT_URL, $url);</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; curl_setopt($curl, CURLOPT_POST, true);</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; curl_setopt($curl, CURLOPT_POSTFIELDS, $json);</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; curl_setopt($curl, CURLOPT_HTTPHEADER, array(&#39;Content-Type: application/json&#39;, &#39;accept: application/json&#39;));</div>
    <div>&nbsp;</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $result = curl_exec($curl);</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; curl_close($curl);</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; }</div>
    <div>&nbsp;</div>
    <div>&nbsp; &nbsp; &nbsp; &nbsp; return $result;</div>
</div>
<hr>
<h3>Ответ</h3>
<p>Ответ сожержит массив в формате JSON</p>
<div class="answer-add-debtor">
    <table align="left" border="1" cellpadding="5" cellspacing="0" class="table">
        <tbody>
        <tr>
            <td><b>Параметр</b></td>
            <td><b>Тип</b></td>
            <td><b>Описание</b></td>
        </tr>
        <tr>
            <td>success</td>
            <td>int</td>
            <td>Успешность операции (0 - ошибка, 1 -&nbsp;операция успешна)</td>
        </tr>
        <tr>
            <td>error</td>
            <td>string</td>
            <td>Описание возникшей ошибки (параметр присутствует только если success = 0)</td>
        </tr>
        <tr>
            <td>data</td>
            <td>array</td>
            <td>Массив с данными об операции</td>
        </tr>
        <tr>
            <td>debtor_id</td>
            <td>int</td>
            <td>Идентификатор должника</td>
        </tr>
        <tr>
            <td>loan_id</td>
            <td>int</td>
            <td>Идентификатор дела</td>
        </tr>
        </tbody>
    </table>
</div>
<p>&nbsp;</p>
<p><b>Пример ответа при ошибке</b>:&nbsp;{&quot;success&quot;:0,&quot;error&quot;:&quot;Description error&quot;}</p>
<p><b>Пример ответа при успешном выполнении:</b></p>
<p>{</p>
<div>&nbsp; &nbsp; &quot;success&quot;: 1,</div>
<div>&nbsp; &nbsp; &quot;data&quot;: {</div>
<div>&nbsp; &nbsp; &nbsp; &nbsp; &quot;debtor_id&quot;: 345,</div>
<div>&nbsp; &nbsp; &nbsp; &nbsp; &quot;loan_id&quot;: 431</div>
<div>&nbsp; &nbsp; }</div>
<div>}</div>
<p>&nbsp;</p>
<hr>
<h2>Получение всех статусов должника</h2>
<h3>Запрос</h3>
<p>Адрес: <?= Yii::$app->request->hostName ?>/api/debtor/get-statuses?token=&lt;токен компании&gt;</p>
<p>Метод: GET</p>
<p>&nbsp;</p>
<h3>Ответ</h3>
<p>Ответ сожержит массив в формате JSON</p>
<div class="answer-add-debtor">
    <table align="left" border="1" cellpadding="5" cellspacing="0" class="table">
        <tbody>
        <tr>
            <td><b>Параметр</b></td>
            <td><b>Тип</b></td>
            <td><b>Описание</b></td>
        </tr>
        <tr>
            <td>success</td>
            <td>int</td>
            <td>Успешность операции (0 - ошибка, 1 -&nbsp;операция успешна)</td>
        </tr>
        <tr>
            <td>error</td>
            <td>string</td>
            <td>Описание возникшей ошибки (параметр присутствует только если success = 0)</td>
        </tr>
        <tr>
            <td>data</td>
            <td>array</td>
            <td>Массив массивов со статусами</td>
        </tr>
        <tr>
            <td>id</td>
            <td>int</td>
            <td>Идентификатор статуса</td>
        </tr>
        <tr>
            <td>name</td>
            <td>string</td>
            <td>Наименование</td>
        </tr>
        </tbody>
    </table>
</div>
<p>&nbsp;</p>
<p><b>Пример ответа</b>:</p>
<div>{</div>
<div>&nbsp; &nbsp; &quot;success&quot;: 1,</div>
<div>&nbsp; &nbsp; &quot;data&quot;: [</div>
<div>&nbsp; &nbsp; &nbsp; &nbsp; {</div>
<div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;id&quot;: 34,</div>
<div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;name&quot;: &quot;status 1&quot;,</div>
<div>&nbsp; &nbsp; &nbsp; &nbsp; },</div>
<div>&nbsp; &nbsp; &nbsp; &nbsp; {</div>
<div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;id&quot;: 35,</div>
<div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;name&quot;: &quot;status 2&quot;,</div>
<div>&nbsp; &nbsp; &nbsp; &nbsp; }</div>
<div>&nbsp; &nbsp; ]</div>
<div>}</div>
<div>
    <hr>
    <h2>Получение списка кредитных продуктов</h2>
    <h3>Запрос</h3>
    <p>Адрес: <?= Yii::$app->request->hostName ?>/api/loan/types?token=&lt;токен компании&gt;</p>
    <p>Метод: GET</p>
    <p>&nbsp;</p>
    <h3>Ответ</h3>
    <p>Ответ сожержит массив в формате JSON</p>
    <div class="answer-add-debtor">
        <table align="left" border="1" cellpadding="5" cellspacing="0" class="table">
            <tbody>
            <tr>
                <td><b>Параметр</b></td>
                <td><b>Тип</b></td>
                <td><b>Описание</b></td>
            </tr>
            <tr>
                <td>success</td>
                <td>int</td>
                <td>Успешность операции (0 - ошибка, 1 -&nbsp;операция успешна)</td>
            </tr>
            <tr>
                <td>error</td>
                <td>string</td>
                <td>Описание возникшей ошибки (параметр присутствует только если success = 0)</td>
            </tr>
            <tr>
                <td>data</td>
                <td>array</td>
                <td>Массив массивов с кредитными продуктами</td>
            </tr>
            <tr>
                <td>id</td>
                <td>int</td>
                <td>Идентификатор продукта</td>
            </tr>
            <tr>
                <td>name</td>
                <td>string</td>
                <td>Наименование</td>
            </tr>
            </tbody>
        </table>
    </div>
    <p>&nbsp;</p>
    <p><b>Пример ответа</b>:&nbsp;</p>
    <div>
        <div>{</div>
        <div>&nbsp; &nbsp; &quot;success&quot;: 1,</div>
        <div>&nbsp; &nbsp; &quot;data&quot;: [</div>
        <div>&nbsp; &nbsp; &nbsp; &nbsp; {</div>
        <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;id&quot;: 34,</div>
        <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;name&quot;: &quot;Кредит &#39;Выгодный&#39;&quot;,</div>
        <div>&nbsp; &nbsp; &nbsp; &nbsp; },</div>
        <div>&nbsp; &nbsp; &nbsp; &nbsp; {</div>
        <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;id&quot;: 35,</div>
        <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;name&quot;: &quot;Кредит для всех&quot;,</div>
        <div>&nbsp; &nbsp; &nbsp; &nbsp; }</div>
        <div>&nbsp; &nbsp; ]</div>
        <div>}</div>
        <div>
            <hr>
            <h2>Получение списка валют.</h2>
            <h3>Запрос</h3>
            <p>Адрес: <?= Yii::$app->request->hostName ?>/api/loan/currencies?token=&lt;токен компании&gt;</p>
            <p>Метод: GET</p>
            <p>&nbsp;</p>
            <h3>Ответ</h3>
            <p>Ответ сожержит массив в формате JSON</p>
            <div class="answer-add-debtor">
                <table align="left" border="1" cellpadding="5" cellspacing="0" class="table">
                    <tbody>
                    <tr>
                        <td><b>Параметр</b></td>
                        <td><b>Тип</b></td>
                        <td><b>Описание</b></td>
                    </tr>
                    <tr>
                        <td>success</td>
                        <td>int</td>
                        <td>Успешность операции (0 - ошибка, 1 -&nbsp;операция успешна)</td>
                    </tr>
                    <tr>
                        <td>error</td>
                        <td>string</td>
                        <td>Описание возникшей ошибки (параметр присутствует только если success = 0)</td>
                    </tr>
                    <tr>
                        <td>data</td>
                        <td>array</td>
                        <td>Массив массивов с валютами</td>
                    </tr>
                    <tr>
                        <td>id</td>
                        <td>int</td>
                        <td>Идентификатор валюты</td>
                    </tr>
                    <tr>
                        <td>name</td>
                        <td>string</td>
                        <td>Наименование</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <p>&nbsp;</p>
            <p><b>Пример ответа</b>:</p>
            <div>{</div>
            <div>&nbsp; &nbsp; &quot;success&quot;: 1,</div>
            <div>&nbsp; &nbsp; &quot;data&quot;: [</div>
            <div>&nbsp; &nbsp; &nbsp; &nbsp; {</div>
            <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;id&quot;: 5,</div>
            <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;name&quot;: &quot;рубль&quot;</div>
            <div>&nbsp; &nbsp; &nbsp; &nbsp; },</div>
            <div>&nbsp; &nbsp; &nbsp; &nbsp; {</div>
            <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;id&quot;: 6,</div>
            <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;name&quot;: &quot;доллар&quot;</div>
            <div>&nbsp; &nbsp; &nbsp; &nbsp; },</div>
            <div>&nbsp; &nbsp; &nbsp; &nbsp; {</div>
            <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;id&quot;: 7,</div>
            <div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;name&quot;: &quot;евро&quot;</div>
            <div>&nbsp; &nbsp; &nbsp; &nbsp; }</div>
            <div>&nbsp; &nbsp; ]</div>
            <div>}</div>
        </div>
    </div>
</div>
<p>&nbsp;</p>
