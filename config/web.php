<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', \app\bootstrap\AppBootstrap::class],
    'defaultRoute' => 'user',
    'timeZone' => 'Europe/Moscow',
    'name' => 'LOT',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'GhAcZ2DFFsfsdfdFFSDfj2hHCv9-XMRK1mi0wYRu29SWwu',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => [

            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.yandex.ru',
//                'username' => '2222@yandex.ru',
//                'password' => 'j112312313',
//                'port' => '465',
//                'encryption' => 'ssl',
//            ],
        ],
        'elephantio' => [
            'class' => 'sammaye\elephantio\ElephantIo',
            'host' => 'http://lot.teo-crm.com:3002'
        ],
        'formatter' => [
            'dateFormat' => 'php:d.m.Y',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => '₽',
            'timeZone' => 'UTC'
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['test'],
                    'logFile' => '@app/runtime/logs/test.log'
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['_error'],
                    'logFile' => '@app/runtime/logs/_error.log'
                ]
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ],
        'api' => [
            'class' => 'app\modules\api\Api',
        ],
    ],
    'language' => 'ru-RU',
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'generators' => [
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => ['My' => '@app/vendor/yiisoft/yii2-gii/generators/crud/admincolor']
            ]
        ],
        'allowedIPs' => ['127.0.0.1'],
    ];
}

return $config;
