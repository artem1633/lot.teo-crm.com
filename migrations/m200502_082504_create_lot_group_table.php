<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lot_group}}`.
 */
class m200502_082504_create_lot_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lot_group}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'color' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lot_group}}');
    }
}
