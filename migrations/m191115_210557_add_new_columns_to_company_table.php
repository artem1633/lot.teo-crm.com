<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%company}}`.
 */
class m191115_210557_add_new_columns_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company', 'email', $this->string()->comment('Email'));
        $this->addColumn('company', 'phone', $this->string()->comment('Телефон'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'email');
        $this->dropColumn('company', 'phone');
    }
}
