<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%lot_structure_group}}`.
 */
class m200503_063435_add_company_id_column_to_lot_structure_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%lot_structure_group}}', 'company_id', $this->integer());

        $this->addForeignKey(
            'fk-lot_structure_group-company_id',
            '{{%lot_structure_group}}',
            'company_id',
            'company',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-lot_structure_group-company_id', '{{%lot_structure_group}}');
        $this->dropColumn('{{%lot_structure_group}}', 'company_id');
    }
}
