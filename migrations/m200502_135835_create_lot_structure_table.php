<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lot_structure}}`.
 */
class m200502_135835_create_lot_structure_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lot_structure}}', [
            'id' => $this->primaryKey(),
            'item_name' => $this->string()->comment('Название'),
            'count' => $this->integer()->defaultValue(1)->comment('Количество'),
            'min_price' => $this->double(2)->defaultValue(1000)->comment('Минимальная стоимость'),
            'max_price' => $this->double(2)->defaultValue(2000)->comment('Максимальная стоимость'),
            'comment' => $this->text()->comment('Комментарий'),
            'group_id' => $this->integer()->comment('Группа'),
            'source_link' => $this->text()->comment('Ссылка на источник'),
            'lot_id' =>$this->integer()->comment('Лот')
        ]);

        $this->addForeignKey(
            'fk-lot_structure-group_id',
            '{{%lot_structure}}',
            'group_id',
            '{{%lot_structure_group}}',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-lot_structure-lot_id',
            '{{%lot_structure}}',
            'lot_id',
            '{{%lot}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lot_structure}}');
    }
}
