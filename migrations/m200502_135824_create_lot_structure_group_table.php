<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lot_structure_group}}`.
 */
class m200502_135824_create_lot_structure_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lot_structure_group}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lot_structure_group}}');
    }
}
