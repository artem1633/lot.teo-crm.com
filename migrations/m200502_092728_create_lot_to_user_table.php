<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lot_to_user}}`.
 */
class m200502_092728_create_lot_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lot_to_user}}', [
            'id' => $this->primaryKey(),
            'lot_id' => $this->integer()->comment('Лот'),
            'user_id' => $this->integer()->comment('Пользователь'),
        ]);

        $this->addForeignKey(
            'fk-lot_to_user-lot_id',
            '{{%lot_to_user}}',
            'lot_id',
            '{{%lot}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-lot_to_user-user_id',
            '{{%lot_to_user}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lot_to_user}}');
    }
}
