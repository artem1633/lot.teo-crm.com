<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lot_status}}`.
 */
class m200502_082456_create_lot_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lot_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'color' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lot_status}}');
    }
}
