<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%lot_status}}`.
 */
class m200503_062457_add_company_id_column_to_lot_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%lot_status}}', 'company_id', $this->integer());

        $this->addForeignKey(
            'fk-lot_status-company_id',
            '{{%lot_status}}',
            'company_id',
            'company',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-lot_status-company_id', '{{%lot_status}}');
        $this->dropColumn('{{%lot_status}}', 'company_id');
    }
}
