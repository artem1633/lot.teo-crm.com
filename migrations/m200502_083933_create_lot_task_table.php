<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lot_task}}`.
 */
class m200502_083933_create_lot_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lot_task}}', [
            'id' => $this->primaryKey(),
            'text' => $this->string(),
            'done' => $this->smallInteger(),
            'lot_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-lot_task-lot_id',
            '{{%lot_task}}',
            'lot_id',
            '{{%lot}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lot_task}}');
    }
}
