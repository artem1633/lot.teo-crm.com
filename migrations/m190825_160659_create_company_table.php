<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m190825_160659_create_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'address' => $this->string()->comment('Адрес'),
            'is_super_company' => $this->boolean()->defaultValue(false)->comment('Является ли супер компанией'),
            'access' => $this->boolean()->defaultValue(true)->comment('Доступ (вкл/выкл)'),
            'last_activity_datetime' => $this->dateTime()->comment('Дата и время последней активности'),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
        ]);

        $this->insert('company', [
            'name' => 'Супер компания',
            'is_super_company' => 1
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('company');
    }
}
