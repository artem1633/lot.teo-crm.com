<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lot}}`.
 */
class m200502_083415_create_lot_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lot}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'description' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->comment('Описание'),
            'pos_num' => $this->integer()->comment('Кол-во позиций'),
            'end_date' => $this->timestamp()->defaultValue(null)->comment('дата окончания этапа/торгов'),
            'auction_link' => $this->text()->comment('Ссылка на торги/ЭТП'),
            'auction_price' => $this->double(2)->comment('Цена на торгах'),
            'rate' => $this->double(2)->comment('Наша ставка'),
            'max_auction_rate' => $this->double(2)->comment('Максимальная цена рынка'),
            'min_auction_rate' => $this->double(2)->comment('Минимальная цена рынка'),
            'sale_profit' => $this->double(2)->comment('Прибыль с продажи'),
            'time_to_realization' => $this->integer()->comment('Время на реаизацию'),
            'client_found' => $this->smallInteger()->comment('Клиент найден'),
            'lot_location' => $this->string()->comment('Местонахождение лота'),
            'opponent_auction_form_num' => $this->integer()->comment('Кол-во заявок оппонентов'),
            'form_is_filing' => $this->smallInteger()->comment('Заявка подана'),
            'deposit_is_payed' => $this->smallInteger()->comment('Задаток оплачен'),
            'tbankrot_link' => $this->text()->comment('Ссылка на tbankrot.ru'),
            'debtor_info' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->comment('Информация о должнике'),
            'stages' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->comment('Этапы'),
            'is_archive' => $this->smallInteger()->comment('Архив'),
            'lot_status_id' => $this->integer()->comment('Статус'),
            'lot_group_id' => $this->integer()->comment('Группа'),
            'competitive_manager_phone' => $this->string()->comment('Телефон конкурсного управлябщего'),
            'competitive_manager_email' => $this->string()->comment('Email конкурсного управляющего'),
            'company_id' => $this->integer()->comment('Компания-создаель')
        ]);

        $this->addForeignKey(
            'fk-lot-lot_status_id',
            '{{%lot}}',
            'lot_status_id',
            '{{%lot_status}}',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-lot-lot_group_id',
            '{{%lot}}',
            'lot_group_id',
            '{{%lot_group}}',
            'id',
            'SET NULL',
            'CASCADE'
        );


        $this->addForeignKey(
            'fk-lot-company_id',
            '{{%lot}}',
            'company_id',
            '{{%company}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lot}}');
    }
}
